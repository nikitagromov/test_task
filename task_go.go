package main

import "fmt"

const deleted = ""


type InMemoryDatabase struct {
	data         map[string]string
	transactions []map[string]string
}


func NewInMemoryDatabase() *InMemoryDatabase {
	return &InMemoryDatabase{
		data:         make(map[string]string),
		transactions: make([]map[string]string, 0),
	}
}

func (db *InMemoryDatabase) Get(key string) (string, bool) {
	for i := len(db.transactions) - 1; i >= 0; i-- {
		if value, ok := db.transactions[i][key]; ok {
			if value == deleted {
				return "", false // Key was deleted
			}
			return value, true
		}
	}
	value, ok := db.data[key]
	return value, ok && value != deleted
}


func (db *InMemoryDatabase) Set(key string, value string) {
	if len(db.transactions) > 0 {
		db.transactions[len(db.transactions)-1][key] = value
	} else {
		db.data[key] = value
	}
}


func (db *InMemoryDatabase) Delete(key string) {
	db.Set(key, deleted)
}


func (db *InMemoryDatabase) StartTransaction() {
	db.transactions = append(db.transactions, make(map[string]string))
}


func (db *InMemoryDatabase) Commit() {
	if len(db.transactions) > 0 {
		lastTransaction := db.transactions[len(db.transactions)-1]
		db.transactions = db.transactions[:len(db.transactions)-1]
		for key, value := range lastTransaction {
			if value == deleted {
				delete(db.data, key)
			} else {
				db.data[key] = value
			}
		}
	}
}

// RollBack rolls back the last transaction
func (db *InMemoryDatabase) RollBack() {
	if len(db.transactions) > 0 {
		db.transactions = db.transactions[:len(db.transactions)-1]
	}
}

func main() {
	db := NewInMemoryDatabase()

	// Example 1: Commit a transaction
	db.Set("key1", "value1")
	db.StartTransaction()
	db.Set("key1", "value2")
	db.Commit()
	if value, ok := db.Get("key1"); !ok || value != "value2" {
		panic("Example 1 failed")
	}

	// Example 2: Roll back a transaction
	db.Set("key1", "value1")
	db.StartTransaction()
	db.Set("key1", "value2")
	db.RollBack()
	if value, ok := db.Get("key1"); !ok || value != "value1" {
		panic("Example 2 failed")
	}
	
	fmt.Println("All examples passed")
}
