class InMemoryDatabase:
    def __init__(self):
        self.data = {}
        self.transactions = []

    def get(self, key):
        for transaction in reversed(self.transactions):
            if key in transaction:
                return transaction[key] if transaction[key] is not None else None
        return self.data.get(key, None)

    def set(self, key, value):
        if self.transactions:
            self.transactions[-1][key] = value
        else:
            self.data[key] = value

    def delete(self, key):
        self.set(key, None)

    def start_transaction(self):
        self.transactions.append({})

    def commit(self):
        if self.transactions:
            transaction = self.transactions.pop()
            for key, value in transaction.items():
                if value is None and key in self.data:
                    del self.data[key]
                elif value is not None:
                    self.data[key] = value

    def roll_back(self):
        if self.transactions:
            self.transactions.pop()


# Example 1 for commit a transaction
db = InMemoryDatabase()
db.set("key1", "value1")
db.start_transaction()
db.set("key1", "value2")
db.commit()
assert db.get("key1") == "value2"

# Example 2 for roll_back()
db = InMemoryDatabase()
db.set("key1", "value1")
db.start_transaction()
assert db.get("key1") == "value1"
db.set("key1", "value2")
assert db.get("key1") == "value2"
db.roll_back()
assert db.get("key1") == "value1"

# Example 3 for nested transactions
db = InMemoryDatabase()
db.set("key1", "value1")
db.start_transaction()
db.set("key1", "value2")
assert db.get("key1") == "value2"
db.start_transaction()
assert db.get("key1") == "value2"
db.delete("key1")
db.commit()
assert db.get("key1") is None
db.commit()
assert db.get("key1") is None

# Example 4 for nested transactions with roll_back()
db = InMemoryDatabase()
db.set("key1", "value1")
db.start_transaction()
db.set("key1", "value2")
assert db.get("key1") == "value2"
db.start_transaction()
assert db.get("key1") == "value2"
db.delete("key1")
db.roll_back()
assert db.get("key1") == "value2"
db.commit()
assert db.get("key1") == "value2"
